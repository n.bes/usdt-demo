#include <hello.pb.h>
#include <hello.grpc.pb.h>
#include <grpc++/grpc++.h>
#include <grpcpp/grpcpp.h>
#include <string>
#include <thread>

#ifdef WITH_USDT_PROBE
#include <sys/sdt.h>
#endif

class hello_api_service_t: public hello_grpc_api::HelloAPI::Service {
public:
    grpc::Status SayHello(
            grpc::ServerContext *context,
            const hello_grpc_api::Message *request_message,
            hello_grpc_api::Message *response_message) override {
        try {
#ifdef WITH_USDT_PROBE
            DTRACE_PROBE(hello_grpc_server, say_hello_begin);
#endif
            if (request_message)
                std::cout << "request_message: " << request_message->value() << std::endl;

            std::this_thread::sleep_for(std::chrono::seconds(1));

            if (response_message)
                response_message->set_value("I'm server");

#ifdef WITH_USDT_PROBE
            DTRACE_PROBE(hello_grpc_server, say_hello_end);
#endif
            return grpc::Status::OK;
        }
        catch (const std::exception &e) {
#ifdef WITH_USDT_PROBE
            DTRACE_PROBE(hello_grpc_server, say_hello_end);
#endif
            return grpc::Status(grpc::StatusCode::UNKNOWN, std::string(e.what()));
        }
    };
};


int main() {
    hello_api_service_t hello_api_service;

    grpc::ServerBuilder builder;
    builder.AddListeningPort( "unix:///var/run/hello.grpc.sock", grpc::InsecureServerCredentials() );
    builder.RegisterService( &hello_api_service );
    std::unique_ptr<grpc::Server> server = builder.BuildAndStart();
    server->Wait();
    return 0;
}
