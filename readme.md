# gRPC client-server
User Statically-Defined Tracing (USDT) probes example.
Inspired by:
* https://b10c.me/blog/008-bitcoin-core-usdt-support/ (https://github.com/0xB10C/bitcoind-observer)
* https://www.brendangregg.com/systems-performance-2nd-edition-book.html

## bpftrace/perf monitoring

```shell
docker build . --target prod -t usdt-demo
docker run --rm -it --privileged usdt-demo
```

```terminal
# ln -s /usr/lib/linux-tools/6.2.0-32-generic/perf /usr/local/bin/perf
# /usr/local/bin/perf buildid-cache --add /usr/local/bin/hello_grpc_server
# /usr/local/bin/perf buildid-cache --add /usr/local/bin/hello_grpc_client

# /usr/local/bin/perf list | grep hello_grpc
  sdt_hello_grpc_client:say_hello_begin              [SDT event]
  sdt_hello_grpc_client:say_hello_end                [SDT event]
  sdt_hello_grpc_server:say_hello_begin              [SDT event]
  sdt_hello_grpc_server:say_hello_end                [SDT event]

# /usr/local/bin/perf probe -f \
  --add sdt_hello_grpc_client:say_hello_begin \
  --add sdt_hello_grpc_client:say_hello_end \
  --add sdt_hello_grpc_server:say_hello_begin \
  --add sdt_hello_grpc_server:say_hello_end

Added new events:
  sdt_hello_grpc_client:say_hello_begin_1 (on %say_hello_begin in /usr/local/bin/hello_grpc_client)
  sdt_hello_grpc_client:say_hello_end_1 (on %say_hello_end in /usr/local/bin/hello_grpc_client)
  sdt_hello_grpc_server:say_hello_begin_2 (on %say_hello_begin in /usr/local/bin/hello_grpc_server)
  sdt_hello_grpc_server:say_hello_end_2 (on %say_hello_end in /usr/local/bin/hello_grpc_server)
  sdt_hello_grpc_server:say_hello_end_2 (on %say_hello_end in /usr/local/bin/hello_grpc_server)

You can now use it in all perf tools, such as:

  perf record -e sdt_hello_grpc_server:say_hello_end_2 -aR sleep 1


# bpftrace -l | grep hello_grpc
tracepoint:sdt_hello_grpc_client:say_hello_begin
tracepoint:sdt_hello_grpc_client:say_hello_begin_1
tracepoint:sdt_hello_grpc_client:say_hello_end
tracepoint:sdt_hello_grpc_client:say_hello_end_1
tracepoint:sdt_hello_grpc_server:say_hello_begin_2
tracepoint:sdt_hello_grpc_server:say_hello_end_2

# bpftrace -l "usdt:/usr/local/bin/hello_grpc_*:*"
usdt:/usr/local/bin/hello_grpc_client:hello_grpc_client:say_hello_begin
usdt:/usr/local/bin/hello_grpc_client:hello_grpc_client:say_hello_end
usdt:/usr/local/bin/hello_grpc_server:hello_grpc_server:say_hello_begin
usdt:/usr/local/bin/hello_grpc_server:hello_grpc_server:say_hello_end
```

(console 1):
```
# bpftrace hello_hist.bt
Attaching 5 probes...
```

(console 2):
```
/usr/local/bin/hello_grpc_server
```

(console 3):
```
root@2a070af84a40:/# /usr/local/bin/hello_grpc_client
Response: I'm server
root@2a070af84a40:/# /usr/local/bin/hello_grpc_client
Response: I'm server
root@2a070af84a40:/# /usr/local/bin/hello_grpc_client
Response: I'm server
```

(console 1):
```
# bpftrace hello_hist.bt
Attaching 5 probes...
^C

@c: count 3, average 1'001'928'741ns, total 3'005'786'224ns
@s: count 3, average 1'000'232'604ns, total 3'000'697'812ns
```


## eBPF monitoring
```
docker build . --target binaries --output ~/haha
```

```sh
vagrant init ubuntu/lunar64
vagrant up
```

![Alt Text](ebpf.gif)
