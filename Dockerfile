FROM ubuntu:23.04 as build_env
RUN apt-get update -y && \
	apt-get install -y \
# 		bpfcc-tools \
		cargo \
		cmake \
		g++ \
		libgrpc++-dev \
		protobuf-compiler-grpc \
		rustc \
		systemtap-sdt-dev
# RUN apt-get install -y libprotobuf-dev



FROM build_env as ssh_build_env
RUN apt-get install -y openssh-server rsync && \
    ssh-keygen -A && \
    echo 'root:root' | chpasswd
# EXPOSE 22
# CMD ["/usr/sbin/sshd","-D"]



FROM build_env as app_env
RUN cargo search --limit 0
# RUN apt-get install -y gnupg
# RUN apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 4052245BD4284CDD
# RUN echo "deb https://repo.iovisor.org/apt/lunar lunar main" | tee /etc/apt/sources.list.d/iovisor.list
RUN apt-get -y install arping bison git cmake dh-python \
	dpkg-dev pkg-kde-tools ethtool flex inetutils-ping iperf \
	libbpf-dev libedit-dev libelf-dev \
	libfl-dev libzip-dev linux-libc-dev libluajit-5.1-dev \
	luajit python3-netaddr python3-pyroute2 python3-setuptools python3
RUN apt-get install -y clang-16 \
	clang-format-16 \
	libclang-16-dev \
	libclang-common-16-dev \
	libclang-cpp16-dev \
	libclang1-16 \
	llvm-16 \
	llvm-16-dev \
	llvm-16-runtime \
	libllvm16 \
	libpolly-16-dev \
	zip
# ENV LLVM_ROOT=/usr/lib/llvm-16

WORKDIR /bcc
RUN git clone --recursive --branch=v0.28.0 https://github.com/iovisor/bcc.git && \
	mkdir bcc/build && \
	cd bcc/build && \
	cmake .. && \
	make -j4 && \
	make install

# RUN apt-get update -y && apt-get install bcc-tools
WORKDIR /app-rs
COPY mon-rs/ebpf ebpf
COPY mon-rs/src src
COPY mon-rs/Cargo.toml .
RUN cargo build --release

WORKDIR /app
COPY CMakeLists.txt .
COPY client client
COPY server server
COPY proto proto
WORKDIR /app/build
RUN cmake .. && \
    cmake --build . --config Release



FROM ubuntu:23.04 as prod
RUN apt-get update -y && \
	apt-get install -y \
		bpfcc-tools \
		bpftrace \
		libgrpc++ \
		linux-tools-common \
		linux-tools-generic
COPY --from=app_env /app/build/client/hello_grpc_client /usr/local/bin/
COPY --from=app_env /app/build/server/hello_grpc_server /usr/local/bin/
COPY --from=app_env /app-rs/target/release/mon /usr/local/bin/




FROM scratch as binaries
COPY --from=app_env /app/build/client/hello_grpc_client /
COPY --from=app_env /app/build/server/hello_grpc_server /
COPY --from=app_env /app-rs/target/release/mon /
# COPY --from=app_env /usr/lib/x86_64-linux-gnu/libgrpc++.so.1.51 /
# COPY --from=app_env /usr/lib/x86_64-linux-gnu/libgpr.so.29 /
# COPY --from=app_env /usr/lib/x86_64-linux-gnu/libprotobuf.so.32 /
# COPY --from=app_env /usr/lib/x86_64-linux-gnu/libabsl_synchronization.so.20220623 /
# /usr/sbin/tplist-bpfcc /usr/local/bin/hello_grpc_client