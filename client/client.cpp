#include <hello.pb.h>
#include <hello.grpc.pb.h>
#include <grpc++/grpc++.h>
#include <grpcpp/grpcpp.h>

#ifdef WITH_USDT_PROBE
#include <sys/sdt.h>
#endif

int main() {
    std::shared_ptr<hello_grpc_api::HelloAPI::StubInterface> grpc = hello_grpc_api::HelloAPI::NewStub(
            grpc::CreateChannel("unix:///var/run/hello.grpc.sock", grpc::InsecureChannelCredentials() ) );

    // todo: use monotonic timer (https://github.com/grpc/grpc/issues/16891#issuecomment-561782971)
    auto deadline = std::chrono::system_clock::now() + std::chrono::seconds(5);
    auto context = std::make_shared<grpc::ClientContext>();
    context->set_deadline(deadline);

    hello_grpc_api::Message request_message;
    request_message.set_value("I'm client");

    hello_grpc_api::Message response_message;

#ifdef WITH_USDT_PROBE
    DTRACE_PROBE(hello_grpc_client, say_hello_begin);
#endif
    const grpc::Status status = grpc->SayHello(context.get(), request_message, &response_message);
#ifdef WITH_USDT_PROBE
    DTRACE_PROBE(hello_grpc_client, say_hello_end);
#endif
    switch (status.error_code()) {
        case grpc::StatusCode::OK: {
            std::cout << "Response: " << response_message.value() << std::endl;
            break;
        }
        default: {
            std::cerr << "RPC error: "
            << status.error_message()
            << '(' <<  status.error_code() << ')' << std::endl;
            return 1;
        }
    }

    return 0;
};
