use bcc::perf_event::PerfMapBuilder;
use bcc::{BPFBuilder, USDTContext};
use std::env;
use log::info;

use simple_logger::SimpleLogger;
const LOG_TARGET: &str = "main";

fn main() {
    let server_path = env::args().nth(1).expect("No server path provided.");
    let client_path = env::args().nth(2).expect("No client path provided.");

    SimpleLogger::new()
        .init()
        .expect("Could not setup logging.");

    let mut server_usdt_ctx = USDTContext::from_binary_path(server_path).unwrap();
    let mut client_usdt_ctx = USDTContext::from_binary_path(client_path).unwrap();

    server_usdt_ctx
        .enable_probe("say_hello_begin", "trace_server_say_hello_begin")
        .unwrap();
    server_usdt_ctx
        .enable_probe("say_hello_end", "trace_server_say_hello_end")
        .unwrap();

    client_usdt_ctx
        .enable_probe("say_hello_begin", "trace_client_say_hello_begin")
        .unwrap();
    client_usdt_ctx
        .enable_probe("say_hello_end", "trace_client_say_hello_end")
        .unwrap();


    let code = concat!(
        "#include <uapi/linux/ptrace.h>",
        "\n\n",
        include_str!("../ebpf/usdt.c"),
    );
    let bpf = BPFBuilder::new(code).unwrap()
        .add_usdt_context(server_usdt_ctx).unwrap()
        .add_usdt_context(client_usdt_ctx).unwrap()
        .build().unwrap();

    let server_begin_events = bpf.table("server_begin_events").unwrap();
    let server_end_events = bpf.table("server_end_events").unwrap();
    let client_begin_events = bpf.table("client_begin_events").unwrap();
    let client_end_events = bpf.table("client_end_events").unwrap();

    let mut server_begin_event_perf_map =
        PerfMapBuilder::new(server_begin_events, server_begin_event_cb)
            .build()
            .unwrap();
    let mut server_end_event_perf_map =
        PerfMapBuilder::new(server_end_events, server_end_event_cb)
            .build()
            .unwrap();

    let mut client_begin_event_perf_map =
        PerfMapBuilder::new(client_begin_events, client_begin_event_cb)
            .build()
            .unwrap();
    let mut client_end_event_perf_map =
        PerfMapBuilder::new(client_end_events, client_end_event_cb)
            .build()
            .unwrap();

    loop {
        server_begin_event_perf_map.poll(1);
        server_end_event_perf_map.poll(1);
        client_begin_event_perf_map.poll(1);
        client_end_event_perf_map.poll(1);
    }
}

fn server_begin_event_cb() -> Box<dyn FnMut(&[u8]) + Send> {
    Box::new(|_| {
        info!(target: LOG_TARGET, "server_begin_event_cb");
    })
}

fn server_end_event_cb() -> Box<dyn FnMut(&[u8]) + Send> {
    Box::new(|_| {
        info!(target: LOG_TARGET, "server_end_event_cb");
    })
}

fn client_begin_event_cb() -> Box<dyn FnMut(&[u8]) + Send> {
    Box::new(|_| {
        info!(target: LOG_TARGET, "client_begin_event_cb");
    })
}

fn client_end_event_cb() -> Box<dyn FnMut(&[u8]) + Send> {
    Box::new(|_| {
        info!(target: LOG_TARGET, "client_end_event_cb");
    })
}
