struct event {
};

BPF_PERF_OUTPUT(server_begin_events);
BPF_PERF_OUTPUT(server_end_events);
BPF_PERF_OUTPUT(client_begin_events);
BPF_PERF_OUTPUT(client_end_events);

int trace_server_say_hello_begin(struct pt_regs *ctx) {
    struct event e = {};
    server_begin_events.perf_submit(ctx, &e, sizeof(e));
    return 0;
};

int trace_server_say_hello_end(struct pt_regs *ctx) {
    struct event e = {};
    server_end_events.perf_submit(ctx, &e, sizeof(e));
    return 0;
};

int trace_client_say_hello_begin(struct pt_regs *ctx) {
    struct event e = {};
    client_begin_events.perf_submit(ctx, &e, sizeof(e));
    return 0;
};

int trace_client_say_hello_end(struct pt_regs *ctx) {
    struct event e = {};
    client_end_events.perf_submit(ctx, &e, sizeof(e));
    return 0;
};
